
/**
 * HubSpot Webhook
 * 
 * @see https://legacydocs.hubspot.com/docs/methods/webhooks/webhooks-overview
 * 
 * Payload example
 * 
 * [{
 *    "eventId": "100",
 *    "subscriptionId": 264849,
 *    "portalId": 8220819,
 *    "occurredAt": 1597783935636,
 *    "subscriptionType": "contact.propertyChange",
 *    "attemptNumber": 0,
 *    "objectId": 123,
 *    "changeSource": "CRM",
 *    "propertyName": "website",
 *    "propertyValue": "sample-value"
 * }]
 * 
 * @param {*} event 
 * @param {*} context 
 * @param {*} callback 
 */
module.exports.handler = function (event, context, callback) {

    const send = (body, code = 200) => {
        callback(null, {
            statusCode: code,
            body: JSON.stringify(body)
        })
    }

    /**
     * Check if the request was made by HubSpot
     * 
     * @see https://legacydocs.hubspot.com/docs/methods/webhooks/webhooks-overview#security
     */
    const validateHash = () => {
        const crypto = require('crypto')

        const HS_APP_CLIENT_SECRET = 'f3d84fbc-35ee-4f5e-98e7-b4603eeb417c';

        const SOURCE_STRING = HS_APP_CLIENT_SECRET + event.body;

        const hash = crypto.createHash('sha256').update(SOURCE_STRING).digest('hex');

        let x_hubspot_signature = event.headers['x-hubspot-signature'];

        console.log({
            'HS_APP_CLIENT_SECRET' : 'f3d84fbc-35ee-4f5e-98e7-b4603eeb417c',
            'SOURCE_STRING' : SOURCE_STRING,
            'hash' : hash,
            'x_hubspot_signature' : x_hubspot_signature
        });

        if( x_hubspot_signature && x_hubspot_signature === hash){
            return true;
        }
        return false;
    }

    if(event.httpMethod === 'POST'){
        let body = JSON.parse(event.body)[0];
        
        if(body){
            if(validateHash()){
                //send update to DWH
                console.log('validation pass')
            }
        }
        else{
            send({'error': 'Invalid request'})
        }
    }

    if(event.httpMethod == 'GET'){
        send({"webhook":"hubspot"});
    }

}

